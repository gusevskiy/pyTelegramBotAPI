# import telebot
#
#
# #TOKEN = '5327970335:AAGrQuz_BnxBUz8G5ZWtSh2ViirtesamX6Q'
#
# bot = telebot.TeleBot(TOKEN, parse_mode=None)
#
# ''' Обработчик сообщений'''
# @bot.message_handler(commands=['start', 'help'])
# def send_welcome(message):
#     bot.reply_to(message, 'Ура заработало')
# '''Функция, декорируемая обработчиком сообщения,
#  может иметь произвольное имя, однако она должна иметь
#  только один параметр (messege).'''
#
#
#
# '''еще один обработчик'''
# @bot.message_handler(func=lambda m: True)
# def echo_all(message):
#     bot.reply_to(message, message.text)
# '''Этот повторяет все входящие текстовые сообщения отправителю.
#  Он использует лямбда-функцию для проверки сообщения.
#   Если лямбда возвращает True, сообщение обрабатывается
#    декорированной функцией. Поскольку мы хотим,
#     чтобы все сообщения обрабатывались этой функцией,
#     мы просто всегда возвращаем True.'''
#
#
#
# bot.infinity_polling()
